var lp = require('leftpad');

var timestamp = function() {
    var d = new Date,
        h = lp(d.getHours().toString(), 2, "0"),
        m = lp(d.getMinutes(), 2, "0"),
        s = lp(d.getSeconds(), 2, "0"),
        l = lp(d.getMilliseconds(), 3, "0")

    return h+":"+m+":"+s+"."+l;
}

var log = function(value) {
    console.log(timestamp() + ":  " + value);
}

var err = function(value) {
    console.error(timestamp() + ":  " + value);
}

module.exports = {
    log: log,
    err: err
}