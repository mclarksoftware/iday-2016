var db = require("../db");
var Color = db.model('Color', {
    name: String,
    hex: String
});

Color.initialize = function() {
    var colors = [
        {name: "Red", hex: "#FF0000"},
        {name: "Yellow", hex: "#FFFF00"},
        {name: "Green", hex: "#00FF00"},
        {name: "Blue", hex: "#0000FF"}];
    colors.forEach(function(item, index){
        Color.findOne({name: item.name}, function(err, doc){
            if (doc === null) {
                doc = new Color({name: item.name, hex: item.hex});
                doc.save(function(err){console.log(err)});
            }
        });
    });
}

module.exports = Color;