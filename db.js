var mongoose = require('mongoose'),
    logger = require('./logger');
//var url = process.env.MONGO_URL || "mongodb://localhost:27017/color-vote";
var url = process.env.MONGO_URL || "mongodb://172.17.0.4:27017/color-vote";

mongoose.connect(url, function(){
    logger.log("MongoDB Connected.");
});
module.exports = mongoose;