var bodyParser = require('body-parser');
var router = require('express').Router();
var static = require('express').static;

router.use(bodyParser.json());
router.use("/api", require('./api'));
router.use("/", static('static'));

module.exports = router;