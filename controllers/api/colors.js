var router = require('express').Router();
var Color = require('../../model/color');

router.get('/color', function (req, res, next) {
    Color.find().exec(function (err, colors) {
        if (err) { return next(err) }
        res.json(colors)
    });
});

module.exports = router;