var router = require('express').Router();

router.use(require('./colors'));
router.use(require('./votes'));
router.use(require('./stats'));

module.exports = router;