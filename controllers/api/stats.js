var router = require('express').Router();
var Vote = require('../../model/vote');
var Color = require('../../model/color');

router.get('/stats', function (req, res, next) {
    Color.find().exec(function(err, colors) {
        Vote.find().exec(function (err, votes) {
            if (err) { return next(err) }

            var map = {}
            var result = [];
            colors.forEach(function(item, index){
                var obj = {name: item.name, count: 0, hex: item.hex};
                map[item.name] = obj;
                result.push(obj);
            });

            votes.forEach(function(item, index){
                map[item.name].count += 1;
            });

            res.json(result)
        });
    });
});

module.exports = router;