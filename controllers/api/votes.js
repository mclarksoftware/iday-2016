var router = require('express').Router();
var Vote = require('../../model/vote');
var logger = require("../../logger");

router.get('/vote', function (req, res, next) {
    Vote.find().exec(function (err, votes) {
        if (err) { return next(err) }
        res.json(votes)
    });
});

router.post('/vote', function(req, res, next) {
    logger.log("Vote cast for " + req.body.name);
    var vote = new Vote({name: req.body.name});
    vote.save(function(err, vote){
        if (err) { return next(err); }
        res.status(201).json(vote);
    });
});

router.get("/vote/reset", function(req, res, next) {
    logger.log("Clearing all votes");
    Vote.remove({}, function(err){
        if (err) { return next(err); }
        res.json("{}");
    });
});

module.exports = router;