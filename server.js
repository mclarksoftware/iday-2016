var express = require('express'),
    db = require("./db"),
    logger = require("./logger"),
    Color = require('./model/color'),
    Vote = require('./model/vote');

Color.initialize();

var app = express();
app.use(require('./controllers'));


var server_port = process.env.NODEJS_PORT || 3000;
var server_ip = process.env.NODEJS_IP || '127.0.0.1';

var server = app.listen(server_port, function () {
    logger.log("Server listening on " + server_ip + ":" + server_port);
});
